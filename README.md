# README #

BlobRL is the beginnings of a small roguelike. The code is based on the [libtcod-python tutorial](http://www.roguebasin.com/index.php?title=Complete_Roguelike_Tutorial,_using_python%2Blibtcod#Credits) by João F. Henriques (a.k.a. Jotaf) from rogue basin. 

It's very bare-bones right now, and doesn't have much of my code in it yet. Currently I am working on de-coupling the different sections of the code, so I can start my own work on it.